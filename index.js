$(function(){
    $("[data-bs-toggle='tooltip']").tooltip();
    $("[data-bs-toggle='popover']").popover();
    $('.carousel').carousel ({
        interval: 3000
    });

    $('#contactoModal').on('show.bs.modal', function (e) {
        console.log('el modal se esta mostrando');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);

    });
    $('#contactoModal').on('shown.bs.modal', function (e) {
        console.log('el modal se mostro');
    });
    $('#contactoModal').on('hide.bs.modal', function (e) {
        console.log('el modal se oculta');
        $('#contactoBtn').prop('disabled', false);
        $('#contactoBtn').removeClass('btn-primary');
        $('#contactoBtn').addClass('btn-outline-success');
    });
    $('#contactoModal').on('hidden.bs.modal', function (e) {
        console.log('el modal se ocultOOO');
    });
});